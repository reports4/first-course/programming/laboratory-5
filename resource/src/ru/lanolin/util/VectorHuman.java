package ru.lanolin.util;

import ru.lanolin.lib.human.Human;
import java.util.Comparator;
import java.util.Vector;

public class VectorHuman extends Vector<Human> {
    
    private boolean invert = false;
    
    public VectorHuman(int initialCapacity) {
        super(initialCapacity);
    }
    
    public VectorHuman() { }
    
    @Override
    public synchronized boolean add(Human human) {
        boolean ret = super.add(human);
        defaultSort();
        return ret;
    }
    
    @Override
    public boolean remove(Object o) {
        boolean ret = super.remove(o);
        defaultSort();
        return ret;
    }
    
    public synchronized void reorder(){ invert = !invert; defaultSort(); }
    
    public synchronized boolean isInvert() { return invert; }
    
    private synchronized void defaultSort(){
        super.sort((h1, h2) -> !invert ? h1.compareTo(h2) : (-1) * h1.compareTo(h2));
    }
}
