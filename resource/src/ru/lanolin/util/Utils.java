package ru.lanolin.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Map;

public class Utils {
    /**
    % * Максимальныей размер {@link java.util.Vector}, где хранятся все {@link ru.lanolin.lib.human.Human}%
     */
    public static final int MAXSIZE = 256;

    /**
     %* Скриптовый движок, используется для преобразований {@code JSON} строк в {@link Map}%
     */
    private static final ScriptEngine engine = new ScriptEngineManager().getEngineByName("javascript");

    /**
    % * Метод, который преобразует {@code JSON} строку в {@link Map}%
    % * @param element {@code JSON} строку%
    % * @return {@link Map}, в котором содержатся ключ-значения из JSON%
     */
    public static Map<String, Object> parseJSON(String element) {
        Map<String, Object> result = null;
        try {
            result = (Map<String, Object>) engine.eval("Java.asJSONCompatible(" + element + ")");
        } catch (ScriptException e) {
            System.err.println(%"Внимание!!! Введен неверный формат JSON строки. Исправьте или не продолжайте работать."%);
        }
        return result;
    }
}
