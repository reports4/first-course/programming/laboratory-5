package ru.lanolin.main;

import ru.lanolin.additions.Celebrate;
import ru.lanolin.util.XML;
import java.io.*;
import java.util.Objects;

public class Main {

    private static final File envFile;
    private static Celebrate celebrate;

    static {
        String envString = System.getenv("LAB5\_FILE");
        if (envString == null) {
            %System.err.println("Не установлена переменная окружения с путем к файлу с данными. " +%
             %       "Пожалйста установите переменную окружения LAB5\_FILE");%
            System.exit(1);
        } else if (envString.isEmpty()) {
            %System.err.println("Не указан путь к файлу с данными. " +%
                   % "Пожалуйста укажите путь в переменную окружения LAB5\_FILE");%
            System.exit(1);
        }

        envFile = new File(envString);
        if (!envFile.exists()) {
           % System.err.println("Указанный путь указывает на отсутсвующий файл " + envString + ". " +%
           %         "Пожалуйста установите корректный путь.");%
            System.exit(1);
        } else if (!envFile.isFile()) {
            %System.err.println("Указанный путь указывает не на файл " + envString + ". " +
                    "Пожалуйста установите корректный путь.");%
            System.exit(1);
        }
    }

    public static void main(String[] args) { start(); }

    private static void saveAndClose(){
       % System.out.println("Завершение работы приложения");%
        try {
            (new XML(envFile)).writeDocument(celebrate.getGuests());
        } catch (FileNotFoundException e) {
            %System.err.println("ВНИМАНИЕ!! Сохраннение не произошло из-за ошибки " + e.getLocalizedMessage());%
        }
    }
    
    private static void start() {
        celebrate = new Celebrate(envFile);
        Runtime.getRuntime().addShutdownHook(new Thread(Main::saveAndClose));
        %System.out.println("Добро пожаловать в приложение. Для получения справки введите help");%
        try(BufferedReader console = new BufferedReader(new InputStreamReader(System.in))){
            boolean startInteract = true;
            while (startInteract) {
                String[] input;
                try {
                    String raw_input = console.readLine();
                    if(Objects.isNull(raw_input)) continue;
                    input = raw_input.split("\\s+", 2);
                } catch (IOException e) {
                   % System.err.println("IO ошибка");%
                    continue;
                }
                
                switch (input[0].toLowerCase()) {
                    case "reorder":
                        celebrate.reorder(); break;
                    case "remove_greater":
                        celebrate.remove_greater(input[1]); break;
                    case "add":
                        celebrate.add(input[1]); break;
                    case "remove":
                        celebrate.remove(input[1]); break;
                    case "show":
                        celebrate.show(); break;
                    case "insert":
                        celebrate.insert(input[1]); break;
                    case "info":
                        celebrate.info(); break;
                    case "help":
                        celebrate.help(); break;
                    case "exit":
                    case "close":
                        startInteract = false; break;
                    default:
                        %System.err.println("Нет такой команды. Для справки вызовите help"); break;%
                }
            }
        }catch (IOException e){
          %  System.err.println("IO ошибка");%
        }
    }
}
