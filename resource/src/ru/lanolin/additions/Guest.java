package ru.lanolin.additions;

import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.human.Human;

public class Guest extends Human {

    public Guest(String name, Feel feel, int quality, int evaluation) { super(name, feel, quality, evaluation); }
    public Guest(String name, Feel feel) { super(name, feel); }
    public Guest() {  }

    @Override
    public void rateOutfit(Human other) {
        %System.out.println(">" + this + " оцениват наряд Гостя " + other.getName() + " на " + other.getRateHash());%
    }
    @Override
    public void great(Human other) {
        %System.out.println(">" + this + " приветвует " + other);%
    }
    @Override
    public void bowOut(Human other) {
        %System.out.println(">" + this + " поклонился " + other);%
    }
}