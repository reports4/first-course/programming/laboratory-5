package ru.lanolin.lib.human;

import ru.lanolin.lib.enums.Feel;

/**
 * Описнаие участника мероприятия
 */
public abstract class Human implements DoHuman, Comparable<Human> {
    /**
     * Имя человека
     */
    private String name;
    /**
     * Настроение человека
     */
    private Feel feel;
    /**
     * Качество наряда на человеке
     */
    private int quality;
    /**
     * Оценка наряда на человека
     */
    private int evaluation;

    public Human(String name, Feel feel, int quality, int evaluation) {
        this(name, feel);
        this.quality = quality;
        this.evaluation = evaluation;
    }
    public Human(String name, Feel feel) {
        this.feel = feel;
        this.name = name;
        this.quality = 1;
        this.evaluation = 0;
    }
    protected Human() {  }

    @Override
    public String toString() {
        return String.format("<human><name>%s</name><fate>%s</fate><quality>%d</quality><evaluation>%d</evaluation></human>",
                name, feel.toString(), quality, evaluation);
    }

    /**
     * Выводит информацию о {@link Human} в приятной виде
     * @return {@link String}
     */
    public String toPrettyString() {
        return String.format("Name: %s, Fate: %s, Quality: %d, Evaluation: %d",
                name, feel.toString(), quality, evaluation);
    }

    @Override
    public int hashCode() {
        int sum = toString().hashCode() + getRateHash();
        if (feel == Feel.CONSTRAINT) sum += 5 * 101;
        else if (feel == Feel.NONE) sum += 2 * 101;
        else if (feel == Feel.OK) sum += 3 * 101;
        return sum;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || this.getClass() != obj.getClass()) return false;
        Human h = (Human) obj;
        return this.feel == h.getFeel() &&
                this.name.equals(h.getName()) &&
                this.quality == h.getQuality() &&
                this.evaluation == h.getEvaluation();
    }

    /**
     * Сравнивает двух {@link Human}. Сначало именя, потом натсроение, потом качество и последнее - оценка
     * @param another {@link Human}
     * @return -1 if {@code this} less {@code another} <br>
     * 0 if {@code this} equals {@code another} <br>
     * 1 if {@code this} grate {@code another}
     */
    @Override
    public int compareTo(Human another) {
        int compareName = this.name.compareTo(another.getName());
        if (compareName != 0) return compareName;
        int compareFeel = this.feel.compareTo(another.getFeel());
        if (compareFeel != 0) return compareFeel;
        int compareQuality = Integer.compare(this.quality, another.getQuality());
        if (compareQuality != 0) return compareName;
        return Integer.compare(this.evaluation, another.getEvaluation());
    }

    /**
     * Специальное число, которое можно использовать для оценки наряда
     * @return {@link Integer} hash, составленный из evaluation и quality
     */
    public int getRateHash() { return evaluation * 31 + quality * 51; }

    /**
     * Оценит наряд другого {@link Human}.
     * @param other {@link Human}
     */
    public abstract void rateOutfit(Human other);

    public Feel getFeel() { return feel; }
    public void setFeel(Feel feel) { this.feel = feel; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public int getQuality() { return quality; }
    public void setQuality(int quality) { this.quality = quality; }
    public int getEvaluation() { return evaluation; }
    public void setEvaluation(int evaluation) { this.evaluation = evaluation; }
}
