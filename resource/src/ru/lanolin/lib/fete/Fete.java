package ru.lanolin.lib.fete;

import ru.lanolin.additions.Guest;
import ru.lanolin.lib.enums.Feel;
import ru.lanolin.lib.human.Human;
import ru.lanolin.util.Utils;
import ru.lanolin.util.VectorHuman;
import ru.lanolin.util.XML;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * %Класс, описывающий Мероприятие%
 */
public abstract class Fete implements DoFete {
    /**
     * %Коллекция, в которой храянтся все {@link Human}%
     */
    protected final VectorHuman guests;
    /**
     * %Дата инициализации коллекции%
     */
    private final Date initialize;

    public Fete(File envFile) {
        this.guests = new VectorHuman(Utils.MAXSIZE);
        this.initialize = new Date();
        load(envFile);
    }

    /**
     *% Метод, который парсит с помощью {@link XML} файл с описанием каждого {@link Human}.%
     *% Всех сущностей загружает в {@link Vector}.%
     * %@param envFile {@link File} файл, в котором хранятся xml данные%
     */
    private void load(File envFile) {
        XML xmlParser = new XML(envFile);
        Vector<Human> guests;
        try {
            guests = xmlParser.readXMLFile();
        } catch (IOException | XMLStreamException e) {
           % System.err.println("Произошла ошибка при чтении файла.\\n Ошибка " + e.getClass().getCanonicalName()%
                  %  + " с сообщением: " + e.getLocalizedMessage());%
            return;
        }
        addParticipants(guests);
    }

    /**
     * %Метод, возвращающий {@link Vector} гостей, присутствующих на мероприятии.%
     * %@return {@link Vector}%
     */
    public Vector<Human> getGuests() { return guests; }

    /**
     * %Специальный мемтод, который создает объект типа {@link Human} по {@link Map}, в котором хранятся%
     * %поля со значениями.%
     * %@param element {@link Map} с возможными полями:% <ul>
     *               % <li>{@code name},{@code fate} - обязательные поля</li>%
     *                %<li>{@code quality},{@code evaluation} - опциональные поля</li>%</ul>
     *% @return соданный объект типа {@link Human} из переданных данных в параметр.%
     */
    private Human construct(Map<String, Object> element) {
        Human h = null;
        if (Objects.nonNull(element))
            if (element.size() == 4) {
                try {
                    h = new Guest(
                            element.get("name").toString(),
                            Feel.valueOf(element.get("fate").toString()),
                            Integer.parseInt(element.get("quality").toString()),
                            Integer.parseInt(element.get("evaluation").toString())
                    );
                } catch (NullPointerException e) {
                    %System.err.println("Не хватает одного из полей или непральное имя поля в ключе");%
                } catch (NumberFormatException e1) {
                    %System.err.println("Внимание, передано значение в поле, требующие числовое значение, " +%
                            %"которое не переводится в числовое");%
                }
            } else if (element.size() == 2 &&
                    element.containsKey("name") && element.containsKey("fate")) {
                try {
                    h = new Guest(element.get("name").toString(), Feel.valueOf(element.get("fate").toString()));
                } catch (NullPointerException e) {
                    %System.err.println("Не хватает одного из полей или непральное имя поля в ключе");%
                } catch (IllegalArgumentException e1) {
                    %System.err.println("В качестве значения Fate было передано такое, которое не существует");%
                }
            }
        return h;
    }

    /**
     *% Команда в консоль: {@code info} <br>%
     *% Выводит в консоль информацию о коллекции, в которой хранятся гости на мероприятии.<br>%
     *% Информация:%<ul>
     *% <li>тип коллекции;</li>%
     *% <li>дата инициализации;</li>%
     * %<li>количесво элементом;</li>%
     *% <li>максимальное количество элементов;</li>%
     * %<li>заполенность коллекции.</li>%</ul>
     */
    public void info() {
        System.out.println("------------------------");
       % System.out.println("Информация о коллекции: ");%
       % System.out.printf("Тип: \%s\\n", guests.getClass().getCanonicalName());%
       % System.out.printf("Время инициализации: \%s\\n", initialize);%
       % System.out.printf("Максимальное количество элементов: \%d\\n", Utils.MAXSIZE);%
       % System.out.printf("Количество элементов в коллекции: \%d\\n", guests.size());%
       % System.out.printf("Заполнение \%3.2f\%\%\\n", (float) guests.size() / Utils.MAXSIZE * 100);%
        System.out.println("------------------------");
    }

    /**
     * Команда в консоль: {@code reorder} <br>
     * Действие: производит сортировку коллекции в обратном порядке.
     */
    public void reorder() {
        guests.reorder();
        System.out.println("Сортировка выполнена. Колекция отсортирована в порядке " +
                (guests.isInvert() ? "убывания" : "возрастания"));
    }
    
    /**
     * Команда в консоль: {@code help} <br>
     */
    public void help(){
        System.out.println("Справка по командам: ");
        System.out.println("{JSON element} - обязательное содержание ключей 'name' и 'fate', " +
                "а также необязательные элементы тип int: 'quality' и 'evaluation'");
        System.out.println("Доступные варианты для ввода в поле 'fate': " + Arrays.asList(Feel.values()).toString());
        System.out.println(">reorder - команда сортирует коллекцию в обратном порядке");
        System.out.println(">info - команда выводит информацию об коллекции");
        System.out.println(">show - команда показывает все элементы коллекции");
        System.out.println(">remove\_greater {JSON element} - команда принимает на вход JSON строку, в которой описан " +
                "элемент с которого будет удалены элемента большие этого");
        System.out.println(">add {JSON element} - команда добавляет в коллекцию объект, составленный из JSON строки.");
        System.out.println(">remove {JSON element} - удаляет элемент из коллекции.");
        System.out.println(">insert {int index} {JSON element} - Добавляет элемент в index позицию.");
        System.out.println(">exit/close - Завершение работы приложения.");
    }
    
    /**
     * Команда в консоль: {@code remove\_greater} {JSON element} <br>
     * Удаляте из коллекции элементы, большие данного
     * @param element {@link String} JSON строка.
     */
    public void remove\_greater(String element) {
        final Human h = construct(Utils.parseJSON(element));
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть удален из коллекцию");
            return;
        }
        if (guests.indexOf(h) != -1) {
            guests.removeIf(human -> h.compareTo(human) < 0);
            System.out.println("Удаление выполнено");
        } else {
            System.err.println("Переданный элемент не может быть найден в коллекции. " +
                    "Либо его нет в коллекции, либо уточните запрос");
        }
    }

    /**
     * Команда в консоль: {@code add} {JSON element} <br>
     * Добавляет элемент в коллецию.
     * При ошибке в JSON строке - данный объект не будет добвален в коллекцию
     * @param element JSON строка, которая должна содержать: <ul>
     *                <li>{@code name},{@code fate} - обязательные поля</li>
     *                <li>{@code quality},{@code evaluation} - опциональные поля</li>
     *                </ul>
     */
    public void add(String element) {
        final Human h = construct(Utils.parseJSON(element));
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть добавлен в коллекцию");
            return;
        }
        guests.add(h);
        System.out.println("Успешно добавлен");
    }

    /**
     * Команда в консоль: {@code remove} {JSON element} <br>
     * По переданным параметрам в JSON виде, удаляется элеммент из коллекции, если это возможно. <br>
     * Чтобы точно удалить элемент из коллекции, нужно передать точные значения полей.
     * @param element {@link String} JSON строка, которая должна содержать: <ul>
     *                <li>{@code name},{@code fate} - обязательные поля</li>
     *                <li>{@code quality},{@code evaluation} - опциональные поля</li></ul>
     */
    public void remove(String element) {
        final Human h = construct(Utils.parseJSON(element));
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть удален из коллекцию");
            return;
        }
        guests.remove(h);
        System.out.println("Успешно удален");
    }

    /**
     * Команда в консоль: {@code show}<br>
     * Такая команда выводит в консоль все элементы в "красивом виде".
     */
    public void show() {
        System.out.println("Вывод всех элементов коллекции: ");
        guests.forEach(h -> System.out.println(h.toPrettyString()));
    }

    /**
     * Команда в консоль: {@code insert} {{@code int} index} {JSON element} <br>
     * По JSON создается элемент типа {@link Human}, и добавляется в позицияю, переданную в {@code index}.
     * @param text {@link String} состоящий из двух частей, разделенные пробелом: {{@code int} index} {{@code JSON} element}
     */
    public void insert(String text) {
        String[] splitText = text.split("\\s+", 2);
        int position;
        try {
            position = Integer.parseInt(splitText[0]);
        } catch (NumberFormatException e) {
            System.err.println("В качестве индекса для вставки элемента было передано невалидное значение. " +
                    "Вставить элемент в коллекцию неудалось");
            return;
        }
        final Human h = construct(Utils.parseJSON(splitText[1]));
        if (Objects.isNull(h)) {
            System.err.println("Внимание, невалидный объект не может быть вставлен в коллекцию");
            return;
        }
        try {
            guests.add(position, h);
            System.out.println("Успешно добавлен");
        } catch (IndexOutOfBoundsException e) {
            System.err.println("В качествее индекса было введено значения, которое находится вне дозволенных границ." +
                    "Дозволенные границы: [0;" + guests.size() + ")");
        }
    }
\}